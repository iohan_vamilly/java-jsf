Anotações gerais


1) Análise & Estruturas
    Foi optado por utilizar o banco de dados PostgresSql devido a análise feita referente ao retorno de uma pesquisa de um livro, seguindo o retorno
    da API oferecida pelo google. O Postgres possibilita a criação de campos JSON em suas colunas, e por já ter utilizado esse padrão anteriormente foi
    optado por esse banco de dados.

    No arquivo .ENV, consta a chave gerada no painel de minha conta google, o que permitiu que o back-end realizasse as devidas requisições a essa API.

    Pela análise, até poderiamos criar relacionamentos entre tabelas, pro exemplo: book & book_author, realizando os devidos relacionamentos 1-n, cada um com seu
    respectivo controller e service. Porém foi uma decisão utilizar o relacionamento json, pois no retorno da API não existia o retorno de um id dos autores por exemplo,
    sendo uma informação relevante apenas para nivel informativo, foi decidido utilizar esse padrão.

    Logo após a instalação foi feito algumas modificações na estrutura do laravel, migrando os model para uma pasta /Models e criado uma pasta /Service para 
    ficar mais visual a localização dos arquivos. Também foi incerido um conceito de herança no que diz respeito aos controllers (BaseController) e aos Models(BaseModel), basicamente esses arquivos foram criados para facilitar a chamada de beginTransaction e para gravar auditoria no campo da tabela Book.

    Não foi necessária a instalção da lib de Cors para essa versão do laravel, já vem com default.

2) 
