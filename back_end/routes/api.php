<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'books'], function(){
    Route::get("search_api_books", 'BookController@getSearchApiBooks');
    Route::get("favorite_books", 'BookController@getFavoriteBooks');
    Route::post("favorite_books", 'BookController@postFavoriteBook');
    Route::delete("favorite_books/{idLaravel}", 'BookController@deleteFavoriteBook');
});

