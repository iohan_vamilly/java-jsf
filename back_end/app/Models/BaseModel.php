<?php
namespace App\Models;

/* Libs */
use App\Exceptions\RuleException;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\Model;

/* Services */
use App\Services\UsefulService;

/**
 * This class is responsible for inserting audit fields without actually
 * having to call when registering the model
 * @author Iohan Camargo
*/
class BaseModel extends Model {

    use ValidatingTrait;
    const CREATED_AT = null;
    const UPDATED_AT = null;

    /**
     * Intercepts the save method to insert the audit fields
     * @return void | RuleException
     */
    public function save(array $options = []) {
        try {
            if (!$this->isValid()) {
                $errors = $this->getErrors()->getMessages();
                $message = "";
                foreach ($errors as $error) {
                    $message .= $error[0] . "\n";
                }
                throw new RuleException($message);
            }
        } catch (\Exception $e) {
            throw $e;
        }

        $class = get_called_class();
        if (!$this->exists) {
            if (!empty($class::CREATED_AT)) {
                $this->attributes[$class::CREATED_AT] = UsefulService::getDate();
            }
        } else {
            if (!empty($class::UPDATED_AT)) {
                $this->attributes[$class::UPDATED_AT] = UsefulService::getDate();
            }
        }
        return parent::save($options);
    }
}
