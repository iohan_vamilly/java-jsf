<?php

namespace App\Models;

use Validator;
use App\Models\BaseModel;

class BookModel extends BaseModel
{
    protected $rules = [
        'id' => 'required|string|max:20',
        'kind' => 'required|string|max:20',
        'etag' => 'required|string|max:20',
        'selfLink' => 'required|string|max:255',
        'previewLink' => 'required|string|max:255',
        'canonicalVolumeLink' => 'required|string|max:255',
        'infoLink' => 'string|max:255',
        'title' => 'required|string|max:255',
        'subtitle' => 'string|max:255',
        'authors' => 'required',
        'publisher' => 'required|string|max:255',
        'publishedDate' => 'required',
        'description' => 'string',
        'pageCount' => 'integer',
        'height' => 'string|max:15',
        'ratingsCount' => 'integer',
        'imageLinks' => 'required',
        'language' => 'required|string|max:10',
        'saleInfo' => 'required',
        'accessInfo' => 'required',
    ];
    // The published date is not a valid date.\nThe field 'averageRating' is mandatory.\nThe field 'ratingsCount' is mandatory.
    protected $validationMessages = [
        'id.required' => "The field 'id' is mandatory.",
        'id.string' => "The field 'id' has to be of type string.",
        'id.max' => "The field 'id' support a maximum of 20 characters.",

        'kind.required' => "The field 'kind' is mandatory.",
        'kind.string' => "The field 'kind' has to be of type string.",
        'kind.max' => "The field 'kind' support a maximum of 20 characters.",

        'etag.required' => "The field 'etag' is mandatory.",
        'etag.string' => "The field 'etag' has to be of type string.",
        'etag.max' => "The field 'etag' support a maximum of 20 characters.",

        'selfLink.required' => "The field 'selfLink' is mandatory.",
        'selfLink.string' => "The field 'selfLink' has to be of type string.",
        'selfLink.max' => "The field 'selfLink' support a maximum of 80 characters.",

        'previewLink.required' => "The field 'previewLink' is mandatory.",
        'previewLink.string' => "The field 'previewLink' has to be of type string.",
        'previewLink.max' => "The field 'previewLink' support a maximum of 80 characters.",

        'canonicalVolumeLink.required' => "The field 'canonicalVolumeLink' is mandatory.",
        'canonicalVolumeLink.string' => "The field 'canonicalVolumeLink' has to be of type string.",
        'canonicalVolumeLink.max' => "The field 'canonicalVolumeLink' support a maximum of 80 characters.",

        'infoLink.string' => "The field 'infoLink' has to be of type string.",
        'infoLink.max' => "The field 'infoLink' support a maximum of 80 characters.",

        'title.required' => "The field 'title' is mandatory.",
        'title.string' => "The field 'title' has to be of type string.",
        'title.max' => "The field 'title' support a maximum of 255 characters.",

        'subtitle.string' => "The field 'subtitle' has to be of type string.",
        'subtitle.max' => "The field 'subtitle' support a maximum of 255 characters.",

        'authors.required' => "The field 'authors' is mandatory.",

        'publisher.required' => "The field 'publisher' is mandatory.",
        'publisher.string' => "The field 'publisher' has to be of type string.",
        'publisher.max' => "The field 'publisher' support a maximum of 255 characters.",

        'description.string' => "The field 'description' has to be of type string.",

        'pageCount.integer' => "The field 'pageCount' has to be of type integer.",

        'height.string' => "The field 'height' has to be of type string.",
        'height.max' => "The field 'height' support a maximum of 15 characters.",

        'ratingsCount.required' => "The field 'ratingsCount' is mandatory.",

        'imageLinks.required' => "The field 'imageLinks' is mandatory.",

        'language.required' => "The field 'language' is mandatory.",
        'language.string' => "The field 'language' has to be of type string.",
        'language.max' => "The field 'language' support a maximum of 10 characters.",

        'saleInfo.required' => "The field 'saleInfo' is mandatory.",

        'accessInfo.required' => "The field 'accessInfo' is mandatory.",

        'dateInsert.datetime' => "The field 'dateInsert' has to be of type datetime.",
    ];

    protected $table = "books";

    protected $primaryKey = "idLaravel";

    protected $fillable = [
        "id",
        "idLaravel",
        "kind",
        "etag",
        "selfLink",
        "previewLink",
        "canonicalVolumeLink",
        "infoLink",
        "title",
        "subtitle",
        "authors",
        "publisher",
        "publishedDate",
        "description",
        "pageCount",
        "height",
        "categories",
        "averageRating",
        "ratingsCount",
        "imageLinks",
        "language",
        "saleInfo",
        "accessInfo",
        "dateInsert",
        "dateUpdated",
        "dateExclusion",
        "isExcluded",
    ];

    public $timestamps = false;

    const CREATED_AT = "dateInsert";
    const UPDATED_AT = "dateUpdated";
}
