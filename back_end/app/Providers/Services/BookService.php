<?php

namespace App\Services;

/* Libs Class */
use App\Exceptions\RuleException;
use Illuminate\Support\Facades\DB;

/* Models */
use App\Models\BookModel;

/* Services */
use App\Services\CurlService;
use App\Services\UsefulService;

class BookService
{
    const urlVolumes = "volumes?";
    const urlApiSearch = "https://www.googleapis.com/books/v1/";

    /**
     * Search books saved as favorites on the laravel base
     * @return Array<BookModel> || Array<[]>
    */
    public static function getFavoriteBooks()
    {
        $books = BookModel::all();

        foreach ($books as $key => $book) {
            $book->categories = json_decode($book->categories, true);
            $book->imageLinks = json_decode($book->imageLinks, true);
            $book->canonicalVolumeLink = json_decode($book->canonicalVolumeLink, true);
            $book->accessInfo = json_decode($book->accessInfo, true);
            $book->saleInfo = json_decode($book->saleInfo, true);
        }

        return $books;
    }

    /**
     * Search books saved as favorites on the laravel base
     * @return Array<BookModel> || Array<[]>
    */
    public static function getSearchApiBooks($request)
    {
        $filter = $request->input('name');

        if(!isset($filter)){
            throw new RuleException("Mandatory search parameter for this request");
        }

        $urlVolumes = BookService::urlVolumes."q={$filter}&";

        $curlService = new CurlService();
        $listBooksApi = $curlService->makeGetRequest(
            BookService::urlApiSearch,
            $urlVolumes,
            env("APP_KEY_GOOGLE_BOOKS")
        );

        $formattedRet = [];
        // dd($listBooksApi);
        foreach ($listBooksApi['items'] as $key => $bookApi) {
            $bookAux = new BookModel();
            $bookAux->id = $bookApi['id'];
            $bookAux->kind = $bookApi['kind'];
            $bookAux->etag = $bookApi['etag'];
            $bookAux->selfLink = $bookApi['selfLink'];
            $bookAux->title = $bookApi['volumeInfo']['title'];
            $bookAux->previewLink = $bookApi['volumeInfo']['previewLink'];
            $bookAux->canonicalVolumeLink = $bookApi['volumeInfo']['canonicalVolumeLink'];
            $bookAux->authors = $bookApi['volumeInfo']['authors'];
            $bookAux->publisher = $bookApi['volumeInfo']['publisher'];
            $bookAux->imageLinks = $bookApi['volumeInfo']['imageLinks'];
            $bookAux->language = $bookApi['volumeInfo']['language'];
            $bookAux->saleInfo = $bookApi['saleInfo'];
            $bookAux->accessInfo = $bookApi['accessInfo'];

            if(array_key_exists("subtitle",$bookApi['volumeInfo'])){
                $bookAux->subtitle = $bookApi['volumeInfo']['subtitle'];
            }

            if(array_key_exists("averageRating",$bookApi['volumeInfo'])){
                $bookAux->averageRating = $bookApi['volumeInfo']['averageRating'];
            }

            if(array_key_exists("ratingsCount",$bookApi['volumeInfo'])){
                $bookAux->ratingsCount = $bookApi['volumeInfo']['ratingsCount'];
            }

            if(array_key_exists("publishedDate",$bookApi['volumeInfo'])){
                $bookAux->publishedDate = $bookApi['volumeInfo']['publishedDate'];
            }
            $formattedArray[] = $bookAux;
        }

        return $formattedArray;
    }

    /**
     * Search books saved as favorites on the laravel base
     * @return BookModel || RuleException
    */
    public static function postFavoriteBook($dataRequest)
    {
        if(!isset($dataRequest['id'])) {
            throw new RuleException("The book id is required for registration...");
        }

        $bookAlreadyFavorit = BookModel::where('id', $dataRequest['id'])->first();

        if ($bookAlreadyFavorit) {
            throw new RuleException("This book is already marked as a favorite");
        }

        $favoriteBook = new BookModel();
        $bookService = new BookService();

        $favoriteBook->fill(
            UsefulService::getFieldsFillableByRequest($dataRequest->all(),
            $favoriteBook->getFillable(),
            true)
        );

        $favoriteBook->save();
        return $favoriteBook;
    }

    /**
     * Search books saved as favorites on the laravel base
     * @return BookModel || RuleException
    */
    public static function deleteFavoriteBook($dataRequest, $idLaravel)
    {
        if(!isset($idLaravel)) {
            throw new RuleException("The book id is required for this operation...");
        }

        $book = BookModel::where('idLaravel', $idLaravel)->first();

        if (!isset($book)) {
            throw new RuleException("The informed book does not exist in the list of favorites");
        }

        $book->delete();
        return true;
    }

}
