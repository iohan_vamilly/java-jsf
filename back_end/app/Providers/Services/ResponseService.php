<?php

namespace App\Services;

use Exception;
use Illuminate\Http\Response;

class ResponseService {

    /**
      * Retorna código de sucesso na operação
      * @param $message
      * @param null $returnData
      * @param string $status
      * @return \Illuminate\Http\JsonResponse
    */

    public static function make($data, $status = '200')
    {
        return response()->json($data, $status);
    }

    /**
      * Padrão de reposta para painel de controle
    */
    public static function success($message, $returnData = null, $status = '200')
    {
        return response()->json(["success" => true, "message" => $message, "data" => $returnData], $status);
    }

    /**
      * Padrão de responsta para aplicação web e mobile
    */
    public static function successForApp($returnData = null, $status = '200')
    {
        return response()->json($returnData, $status);
    }

    /**
      * Instrução para retorno de excessões conhecidas - tratadas
      * @param $message
      * @param null $returnData
      * @param string $status
      * @return \Illuminate\Http\JsonResponse
    */
    public static function faillure($message, $returnData = null, $status = '500')
    {
        return response()->json(["success" => false, "message" => $message, "data" => $returnData], $status);
    }

    /**
      * Instrução para retorno de excessões desconhecidas
      * @param $message
      * @param Exception $exception
      * @param String $status
      * @return \Illuminate\Http\JsonResponse
    */
    public static function error(Exception $exception, $status = '500')
    {
        return response()->json([
            "success" => false,
            "message" => $exception->getMessage(),
            "data" => [
              "code" => $exception->getCode(),
              "message" => $exception->getMessage(),
                "trace" => $exception->getTraceAsString()
            ],
            "unknow_exception" => true], $status);
   }

}
