<?php
namespace App\Services;

/* Libs Class */
use App\Exceptions\RuleException;

/**
 * In this generic class there are methods used throughout the system
 * @author Iohan Camargo
*/
class UsefulService
{
    /**
     * Return the current date
     * @param  String $format
     * @return Date   $format
     */
    public static function getDate($format = 'Y-m-d H:i:s')
    {
        return date($format);
    }

    /**
     * From the obtained request, the information is checked and array with fields of class is returned
     * @param Array $dataRequest: The array data already cast with method ->all()
     * @param Array $fieldsModel: The fillable fields from the model
     */
    public static function getFieldsFillableByRequest($dataRequest, $fieldsModel)
    {
        $fieldsFillable = array();

        foreach($dataRequest as $key => $field) {
          if (is_array($field) && !in_array($key,$fieldsModel)) {
              $fieldsFillable = array_merge($fieldsFillable, UsefulService::getFieldsFillableByRequest($field,$fieldsModel));
          }
          else {
              if(is_array($field)){
                  $fieldsFillable[$key] = json_encode($field);
              } else {
                  $fieldsFillable[$key] = $field;
              }
          }
        }
        return $fieldsFillable;
    }

}
