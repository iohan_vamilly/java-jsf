<?php
namespace App\Services;

/* Libs Class */
use App\Exceptions\RuleException;

class CurlService {

    const METHOD_GET = 'GET';
    const METHOD_PUT = 'PUT';
    const METHOD_POST = 'POST';
    const METHOD_DELETE = 'DELETE';

    /**
     * Performs the execution of a request through the lib CURL
     * @param  String $urApi -> Google url apis start
     * @param  String $urlRota -> Complement of google apis url
     * @param  String $apiKey -> Api from google books
     * @return Array<> $ret -> Returns the search performed on google api books
    */
    public static function makeGetRequest($urlApi, $urlRota, $apiKey)
    {
        $urlSearch = $urlApi.$urlRota."key={$apiKey}";
        $curlRequest = curl_init();
        if (!$curlRequest) {
            throw new RuleException(curl_error($curlRequest));
        }

        $options = array(
            CURLOPT_URL => $urlSearch,
            CURLOPT_POST => 0,
            CURLOPT_FRESH_CONNECT => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_CONNECTTIMEOUT => 900,
            CURLOPT_CUSTOMREQUEST => CurlService::METHOD_GET,
            CURLOPT_HTTPHEADER => [
                "Content-Type: application/json"
            ]
        );

        curl_setopt_array($curlRequest, $options);
        $result = curl_exec($curlRequest);

        $info = curl_getinfo($curlRequest);
        $ret = is_string($result) ? json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $result), true ) : $result;
        if ($info['http_code'] != 200) {
            $messageException = "\nURL: {$urlSearch} \nMétodo: GET";
            $messageException .= "\nError making request for api google books {$info['http_code']}." . curl_error($curlRequest);
            throw new RuleException($messageException);
        }

        return $ret;
    }

}

