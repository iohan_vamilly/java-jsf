<?php

namespace App\Http\Controllers;

/* Libs */
use Illuminate\Support\Facades\DB;

/* Services */
use App\Services\ResponseService;

class BaseController extends Controller {

    public static function saveWithBeginTransactionAndCommit($callback){
        try {
            DB::beginTransaction();
            $return = $callback();
            DB::commit();
            return $return;
        }catch (RuleException $ex) {
            DB::rollback();
            return ResponseService::faillure($ex->getMessage(), "500");
        }catch (\Exception $ex) {
            DB::rollback();
            return ResponseService::error($ex);
        }
    }

}
