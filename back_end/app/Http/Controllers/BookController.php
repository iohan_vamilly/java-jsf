<?php
namespace App\Http\Controllers;

/* Lib Class */
use Illuminate\Http\Request;

/* Providers */
use App\Services\BookService;
use App\Services\ResponseService;

/* Controller Class */
use App\Http\Controllers\BaseController;

class BookController extends BaseController
{
    /**
      * Route to web/mobile application responsible for listing favorite books
      * @param $request Request -> GET REQUEST
      * @return Array<object> -> BookModel
    */
    public function getFavoriteBooks(Request $request)
    {
        $books = BookService::getFavoriteBooks();
        return ResponseService::success('Requisition of list of books successfully completed',$books);
    }

    /**
      * Route responsible for conducting the research with the integration of the google books api
      * @param $request Request -> GET REQUEST
      * @return Array<object> -> BookModel
    */
    public function getSearchApiBooks(Request $request)
    {
        $books = BookService::getSearchApiBooks($request);
        return ResponseService::success('Request for list of books with api google books integration completed successfully', $books);
    }

    /**
      * Route to web/mobile application responsible for register one favorite book
      * @param $request Request -> GET REQUEST
      * @return Array<object> -> BookModel
    */
    public function postFavoriteBook(Request $request)
    {
        return $this->saveWithBeginTransactionAndCommit(function () use ($request) {
            $book = BookService::postFavoriteBook($request);
            return ResponseService::success("Book successfully registered as a favorite",$book);
        });
    }

    /**
      * Route to web/mobile application responsible for register one favorite book
      * @param $request Request -> GET REQUEST
      * @return Array<object> -> BookModel
    */
    public function deleteFavoriteBook(Request $request, $idLaravel)
    {
        return $this->saveWithBeginTransactionAndCommit(function () use ($request, $idLaravel) {
            BookService::deleteFavoriteBook($request, $idLaravel);
            return ResponseService::success("Book successfully removed from favorites list",[]);
        });
    }

}
