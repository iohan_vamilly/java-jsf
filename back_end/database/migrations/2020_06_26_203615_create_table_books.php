<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableBooks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('idLaravel');
            $table->string('id', 20)->unique();
            $table->string('kind', 20);
            $table->string('etag', 20);
            $table->string('selfLink', 255);
            $table->string('title');
            $table->string('subtitle')->nullable();
            $table->json('authors');
            $table->string('publisher');
            $table->string('publishedDate')->nullable();
            $table->text('description')->nullable();
            $table->integer('pageCount')->nullable();
            $table->string('height', 15)->nullable();
            $table->json('categories')->nullable();
            $table->float('averageRating',30,2)->default(0.0);
            $table->integer('ratingsCount')->default(0);
            $table->json('imageLinks');
            $table->string('language', 10);
            $table->string('previewLink', 255);
            $table->string('infoLink', 255)->nullable();
            $table->string('canonicalVolumeLink', 255);
            $table->json('saleInfo');
            $table->json('accessInfo');
            $table->dateTime('dateInsert');
            $table->dateTime('dateUpdated')->nullable();
            $table->dateTime('dateExclusion')->nullable();
            $table->integer('isExcluded')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
