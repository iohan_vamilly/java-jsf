/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable react/destructuring-assignment */
import { Modal, Button } from 'react-bootstrap';

import React from 'react';

interface PropertiesModal {
  title: string;
  message: string;
  onConfirm: any;
  textCancel: string;
  textConfirm: string;
  onHide: any;
  show: any;
}

const ConfirmacaoModal: React.FC<PropertiesModal> = (
  props: PropertiesModal,
) => {
  const { title, message, onConfirm, textCancel, textConfirm, ...rest } = props;
  return (
    <Modal
      centered
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      {...rest}
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          {title || 'Attention'}
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <p>{message || 'Você deseja realmente continuar ?'}</p>
      </Modal.Body>

      <Modal.Footer>
        <Button variant="secondary" onClick={props.onHide}>
          {textCancel || 'No'}
        </Button>

        <Button variant="danger" onClick={onConfirm}>
          {textConfirm || 'Yes'}
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default ConfirmacaoModal;
