export interface ImageLinks {
  smallThumbnail: string;
  thumbnail: string;
  small: string;
  medium: string;
  large: string;
}

export interface Epub {
  isAvailable: boolean;
}

export interface Pdf {
  isAvailable: boolean;
}

export interface SalesInfo {
  country: string;
  saleability: string;
  isEbook: boolean;
}

export interface AccessInfo {
  country: string;
  saleability: string;
  isEbook: boolean;
  viewability: string;
  embeddable: boolean;
  publicDomain: boolean;
  textToSpeechPermission: string;
  webReaderLink: string;
  accessViewStatus: string;
  quoteSharingAllowed: boolean;
  epub: Epub;
  pdf: Pdf;
}

export interface Books {
  idLaravel: number;
  id: string;
  kind: string;
  etag: string;
  selfLink: string;
  title: string;
  subtitle: string;
  authors: Array<string>;
  publisher: string;
  publishedDate: string;
  description: string;
  pageCount: number;
  height: string;
  categories: Array<string>;
  averageRating: string;
  ratingsCount: number;
  imageLinks: ImageLinks;
  language: string;
  previewLink: string;
  infoLink: string;
  canonicalVolumeLink: string;
  saleInfo: Array<SalesInfo>;
  isExcluded: boolean;
  dateExclusion: string;
  dateUpdated: string;
  dateInsert: string;
  accessInfo: Array<AccessInfo>;
}
