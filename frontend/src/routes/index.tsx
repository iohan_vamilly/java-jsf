import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Dashboard from '../pages/Dashboard';
import SearchBook from '../pages/SearchBook';

const Routes: React.FC = () => (
  <Switch>
    <Route path="/" exact component={Dashboard} />
    <Route path="/search_books" component={SearchBook} />
  </Switch>
);

export default Routes;
