/* eslint-disable react/jsx-no-target-blank */
import React, { useState, useEffect } from 'react';
import { FiChevronRight } from 'react-icons/fi';
import { Container } from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import { Books } from '../../models/BookModels';
import api from '../../services/api';
import { Title, BooksStyle, Hr } from './styles';
import ModalConfirmation from '../../components/ModalConfirmation/ModalConfirmation';

const Dashboard: React.FC = () => {
  const [msgError, setMsgError] = useState('');
  const [modalOpen, setModalState] = useState(false);
  const [favoriteBooks, setFavoriteBooks] = useState<Books[]>([]);
  const [bookSelected, setBookSelected] = useState<Books | null>(null);

  useEffect(() => {
    api.get(`books/favorite_books`).then((response) => {
      setFavoriteBooks(response.data.data);
    });
  }, []);

  const handleOpenModal = (book: Books) => {
    setBookSelected(book);
    setModalState(true);
  };

  async function handleExcludeFavoritBook(): Promise<void> {
    try {
      if (bookSelected != null) {
        await api.delete(`books/favorite_books/${bookSelected.idLaravel}`);

        const newBooks = favoriteBooks.filter(
          (bookFilter) => bookFilter.idLaravel !== bookSelected.idLaravel,
        );
        setFavoriteBooks(newBooks);
      }
      setModalState(false);
    } catch (err) {
      setMsgError(`Error when deleting the book as a favorite ${err}`);
      setModalState(false);
    }
  }
  return (
    <>
      <Container>
        <Title>Your favorite books here!</Title>
        {msgError && <h3>msgError </h3>}

        <ModalConfirmation
          title="Attention"
          show={modalOpen}
          onConfirm={() => handleExcludeFavoritBook()}
          textCancel="Cancel"
          message="If you delete the book it will no longer be bookmarked. Do you really want to delete? ?"
          textConfirm="Remove"
          onHide={() => setModalState(false)}
        />
        {favoriteBooks.length > 0 &&
          favoriteBooks.map((book) => (
            <>
              <Hr />
              <BooksStyle>
                <img src={book.imageLinks.smallThumbnail} alt={book.title} />
                <a key={book.id} target="_blank" href={book.previewLink}>
                  <div>
                    <FiChevronRight size={20} />
                    <strong>{book.title}</strong>
                    <p>{book.subtitle}</p>
                  </div>
                </a>
                <Button variant="danger" onClick={() => handleOpenModal(book)}>
                  Remove
                </Button>
              </BooksStyle>
              <Hr />
            </>
          ))}
      </Container>
    </>
  );
};

export default Dashboard;
