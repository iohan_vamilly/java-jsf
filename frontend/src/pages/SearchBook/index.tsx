import React, { useEffect, useState, FormEvent } from 'react';
import { useRouteMatch, Link } from 'react-router-dom';
import { FiChevronLeft, FiChevronRight } from 'react-icons/fi';
import { Container } from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import { Error, Success, BooksStyle, Title, Form, Hr } from './styles';
import logoImg from '../../assets/logo.svg';
import api from '../../services/api';
import { Books } from '../../models/BookModels';

interface Repository {
  full_name: string;
  description: string;
  forks_count: number;
  stargazers_count: number;
  open_issues_count: number;
  owner: {
    login: string;
    avatar_url: string;
  };
}

const SearchBook: React.FC = () => {
  const [inputError, setInputError] = useState('');
  const [inputSuccess, setInputSuccess] = useState('');
  const [searchWord, setSearchWord] = useState('');

  // const [newRepo, setSearchWord] = useState('');
  const [books, setSearchBooks] = useState<Books[]>([]);

  async function handleSearch(
    event: FormEvent<HTMLFormElement>,
  ): Promise<void> {
    event.preventDefault();

    if (!searchWord) {
      setInputError('You must type a word to search ...');
      return;
    }

    try {
      const response = await api.get(
        `books/search_api_books?&name=${searchWord}`,
      );
      // console.log(response.data);
      const booksSearch = response.data.data;
      console.log(booksSearch);
      setSearchBooks(booksSearch);
      setSearchWord('');
      setInputError('');
    } catch (err) {
      setInputError('Erro na busca por esse repositório');
    }
  }

  async function handleAddToFavorites(book: Books): Promise<void> {
    try {
      await api.post(`books/favorite_books`, book);
      setInputSuccess('The book was successfully registered');
    } catch (err) {
      setInputSuccess('This book is already a favorite');
    }
  }

  return (
    <>
      <Container>
        <Title>Search Books</Title>
        {books.length === 0 && (
          <h3>
            There are no books registered as favorites, to add click on the
            search books menu...
          </h3>
        )}
        <Form hasError={!!inputError} onSubmit={handleSearch}>
          <input
            value={searchWord}
            onChange={(e) => setSearchWord(e.target.value)}
            placeholder="Type the name of the book to search"
          />
          <button type="submit">Search</button>
        </Form>
        {inputError && <Error>{inputError}</Error>}
        {inputSuccess && <Success>{inputSuccess}</Success>}
        {books.length > 0 &&
          books.map((book) => (
            <>
              <Hr />
              <BooksStyle>
                <img src={book.imageLinks.smallThumbnail} alt={book.title} />
                <a key={book.id} target="_blank" href={book.previewLink}>
                  <div>
                    <FiChevronRight size={20} />
                    <strong>{book.title}</strong>
                    <p>{book.subtitle}</p>
                  </div>
                </a>
                <Button
                  variant="success"
                  onClick={() => handleAddToFavorites(book)}
                >
                  Add to favorites
                </Button>
              </BooksStyle>
              <Hr />
            </>
          ))}
      </Container>
    </>
  );
};

export default SearchBook;
