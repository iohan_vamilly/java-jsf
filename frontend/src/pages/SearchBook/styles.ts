import styled, { css } from 'styled-components';
import { shade } from 'polished';

// export const Container = styled.div`
//   justify-content: center;
//   align-items: center;
//   padding: 40px 20px;
// `;
interface FormProps {
  hasError: boolean;
}
export const Title = styled.h1`
  font-size: 48px;
  color: #3a3a3a;
  line-height: 56px;
  margin-top: 30px;
`;

export const Hr = styled.hr`
  color: red;
  background-color: white;
`;

export const Form = styled.form<FormProps>`
  margin-top: 40px;
  /* max-width: 700px; */
  display: flex;

  input {
    flex: 1;
    height: 70px;
    padding: 0 24px;
    border: 0;
    border-radius: 5px 0 0 5px;
    color: #3a3a3a;
    border: 2px solid #fff;
    border-right: 0;
    ${(props) =>
      props.hasError &&
      css`
        border-color: #c53030;
      `}

    &::placeholder {
      color: #a8a8b3;
    }
  }

  button {
    width: 210px;
    height: 70px;
    background: #04d361;
    border-radius: 0px 5px 5px 0px;
    border: 0;
    color: #fff;
    font-weight: bold;
    transition: background-color 0.2s;

    &:hover {
      background: ${shade(0.2, '#04d361')};
    }
  }
`;

export const Error = styled.span`
  display: block;
  color: #c53030;
  margin-top: 8px;
`;

export const Success = styled.span`
  display: block;
  color: #04d361;
  margin-top: 8px;
`;

export const BooksStyle = styled.div`
  background: #fff;
  border-radius: 5px;
  width: 100%;
  padding: 24px;
  display: block;
  text-decoration: none;
  display: flex;
  align-items: center;
  transition: transform 0.2s;
  img {
    width: 164px;
    height: 164px;
    border-radius: 50%;
    margin-right: 15px;
  }

  div {
    margin: 0xp 16px;
    flex: 1;

    strong {
      font-size: 20px;
      color: #3d3d4d;
    }

    p {
      font-size: 18px;
      color: #a8a8b3;
      margin-top: 4px;
    }
  }
  a {
    margin-right: auto;

    &:hover {
      transform: translateX(10px);
    }

    svg {
      margin-left: auto;
      color: #cbcbd6;
    }
    & + a {
      margin-top: 20px;
      margin-right: 20px;

      svg {
        color: red;
      }
    }
  }
`;
