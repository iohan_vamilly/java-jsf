import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import Routes from './routes';
import 'bootstrap/dist/css/bootstrap.min.css';
import GlobalStyle from './styles/global';
import Menu from './components/Menu';

const App: React.FC = () => (
  <>
    <BrowserRouter>
      <Menu />
      <Routes />
    </BrowserRouter>
    <GlobalStyle />
  </>
);

export default App;
